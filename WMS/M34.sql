﻿
USE wms_dev
GO
INSERT INTO wms.Ref
(
    TableName,
    ColumnName,
    Code,
    Description,
    OrderNum,
    IsActive
)
SELECT A.TableName,
       A.ColumnName,
       A.Code,
       A.Description,
       A.OrderNum,
       A.IsActive
FROM (
	SELECT 'wms.SoRequest' AS TableName,'StatusCode' AS ColumnName,'DRAFT' AS Code,N'Tạm' AS Description,1 OrderNum,1 AS IsActive
	UNION ALL
	SELECT 'wms.SoRequest' AS TableName,'StatusCode' AS ColumnName,'PREVIEW' AS Code,N'Trình duyệt' AS Description,5 OrderNum,1 AS IsActive
	UNION ALL
	SELECT 'wms.SoRequest' AS TableName,'StatusCode' AS ColumnName,'APPROVED' AS Code,N'Duyệt' AS Description,10 OrderNum,1 AS IsActive
	UNION ALL
	SELECT 'wms.SoRequest' AS TableName,'StatusCode' AS ColumnName,'PROCESSING' AS Code,N'Đang xử lý' AS Description,15 OrderNum,1 AS IsActive
	UNION ALL
	SELECT 'wms.SoRequest' AS TableName,'StatusCode' AS ColumnName,'FINISHED' AS Code,N'Hoàn thành' AS Description,15 OrderNum,1 AS IsActive
	UNION ALL
	SELECT 'wms.SoRequest' AS TableName,'StatusCode' AS ColumnName,'CANCELED' AS Code,N'Hủy' AS Description,25 OrderNum,1 AS IsActive
	UNION ALL
	SELECT 'wms.SoRequest' AS TableName,'StatusCode' AS ColumnName,'REJECTED' AS Code,N'Từ chối' AS Description,25 OrderNum,1 AS IsActive


)A
	LEFT JOIN wms.Ref B ON B.Code = A.Code
						   AND B.TableName = A.TableName
						   AND B.ColumnName = A.ColumnName
WHERE B.TableName IS NULL

GO
INSERT INTO wms.RefType
(
    Code,
    Name
)
VALUES
( 'SOREQ', N'So Request' )

INSERT INTO wms.ActionType
(
    Code,
    Name,
    TransType
)

VALUES
( 'SOREQ_CRT', N'Sales Order Request Created', 'WaitingOut' ),
( 'SOREQ_CNCL', N'Sales Order Request Cancel', 'WaitingOut' )

INSERT INTO wms.RefType
(
    Code,
    Name
)
SELECT'DN', N'Delivery Note' UNION ALL
SELECT'PORC', N'Po receipt' UNION ALL
SELECT'RO', N'Return order' 



INSERT INTO wms.Ref
(
    TableName,
    ColumnName,
    Code,
    Description,
    OrderNum,
    IsActive
)
SELECT A.TableName,
       A.ColumnName,
       A.Code,
       A.Description,
       A.OrderNum,
       A.IsActive
FROM (
	SELECT 'wms.SoRequestStatusTracking' AS TableName,'ActionTypeCode' AS ColumnName,'ADD' AS Code,N'Tạo mới' AS Description,1 OrderNum,1 AS IsActive
	UNION ALL
	SELECT 'wms.SoRequestStatusTracking' AS TableName,'ActionTypeCode' AS ColumnName,'UPD' AS Code,N'Cập nhật' AS Description,5 OrderNum,1 AS IsActive
	UNION ALL
	SELECT 'wms.SoRequestStatusTracking' AS TableName,'ActionTypeCode' AS ColumnName,'DEL' AS Code,N'Xóa' AS Description,10 OrderNum,1 AS IsActive
	UNION ALL
	SELECT 'wms.SoRequestStatusTracking' AS TableName,'ActionTypeCode' AS ColumnName,'PRE' AS Code,N'Trình duyệt' AS Description,15 OrderNum,1 AS IsActive
	UNION ALL
	SELECT 'wms.SoRequestStatusTracking' AS TableName,'ActionTypeCode' AS ColumnName,'RJT' AS Code,N'Từ chối' AS Description,15 OrderNum,1 AS IsActive
)A
	LEFT JOIN wms.Ref B ON B.Code = A.Code
						   AND B.TableName = A.TableName
						   AND B.ColumnName = A.ColumnName
WHERE B.TableName IS NULL

INSERT INTO wms.ActionType
(
    Code,
    Name,
    TransType
)

VALUES
( 'SOREQ_RJT', N'Sales Order Request Rejected', 'WaitingOut' ),
( 'SOREQ_CPTD', N'Sales Order Request Completed', 'WaitingOut' )

INSERT INTO wms.Ref
(
    TableName,
    ColumnName,
    Code,
    Description,
    OrderNum,
    IsActive
)
SELECT A.TableName,
       A.ColumnName,
       A.Code,
       A.Description,
       A.OrderNum,
       A.IsActive
FROM (
	SELECT 'dms.Po' AS TableName,'StatusCode' AS ColumnName,'NEW' AS Code,N'Mới' AS Description,1 OrderNum,1 AS IsActive
	UNION ALL
	SELECT 'dms.Po' AS TableName,'StatusCode' AS ColumnName,'RECEIVING' AS Code,N'Đang nhận hàng' AS Description,5 OrderNum,1 AS IsActive
	UNION ALL
	SELECT 'dms.Po' AS TableName,'StatusCode' AS ColumnName,'FINISHED' AS Code,N'Hoàn thành' AS Description,10 OrderNum,1 AS IsActive

)A
	LEFT JOIN wms.Ref B ON B.Code = A.Code
						   AND B.TableName = A.TableName
						   AND B.ColumnName = A.ColumnName
WHERE B.TableName IS NULL


/* test */